import math


def calculator(a, b, operator):
    # ==============
    # Your code here
    result = 0
    if (operator == "+"):
        result = a + b
    elif (operator == "-"):
        result = a - b
    elif (operator == "*"):
        result = a * b
    elif (operator == "/"):
        result = a / b
    result = math.floor(result)

    twopower = 1
    while twopower * 2 < result:
        twopower = twopower * 2

    reduction = result
    result = 0
    while twopower >= 1:
        if (reduction >= twopower):
            reduction = reduction - twopower
            result = result + 10**(math.log(twopower, 2))
        twopower = twopower / 2
    return math.floor(result)
    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
